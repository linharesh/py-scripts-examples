
def routine_a(num):
    return num+20

def routine_b(num):
    return num/2

def routine_c(num):
    x = routine_x(num)
    return routine_y(x)

def routine_x(num):
    return num + 1

def routine_y(num):
    return num - (num/2)

def routine_d(num):
    return num+7


def main():
    my_variable = 10
    x = routine_a(my_variable)
    x = routine_b(x)
    res = routine_c(x)
    result = routine_d(res) 
    print(result)
    
if __name__ == "__main__":
    main()
