import csv

FILE_NAME = 'MOCK_DATA.csv'

def readfile(filename):
    sample = []
    myfile = open(filename)
    with myfile as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            sample.append(int(row[0]))
    return sample

def get_max(data):
    current = data[0]
    for d in data:
        if d > current:
            current = d
    return current

def get_sum(data):
    return sum(data)

def get_avg(data):
    return get_sum(data)/len(data) 

def print_results(maximum,summ,avg):
    print("maximum: {}".format(maximum))
    print("sum: {}".format(summ))
    print("avg: {}".format(avg))

data = readfile(FILE_NAME)
maximum = get_max(data)
summ = get_sum(data)
avg = get_avg(data)
print_results(maximum,summ,avg)
