import csv
from student import Student
from classroom_statistics import ClassroomStatistics

FILE_NAME = 'MOCK_DATA.csv'

def readfile(myfile):
    rawdata = []
    myfile.readline()
    for line in myfile:
        rawdata.append(line)
    return rawdata

def parse_student(strings):
    s_id = strings[0]
    name = strings[1]
    first_exam = float(strings[2])
    second_exam = float(strings[3])
    avg = float(strings[4])
    if (strings[5]) == "True":
        situation = True
    else:
        situation = False
    return Student(s_id, name, first_exam, second_exam, avg, situation)


def process_data(rawdata:[]):
    students = []
    for row in rawdata:
        std = parse_student(row.split(','))
        students.append(std)
    return students


def validate_avgs(data):
    for student in data:
        if student.avg != ((student.first_exam + student.second_exam) % 2):
            return False
    return True


def validate_situation(data):
    for student in data:
        if student.avg >= 6 and student.situation == False:
            return False
        if student.avg < 6 and student.situation == True:
            return False
    return True

def generate_class_report(data):
    f = open("class_grades.txt", "w")
    for student in data:
        if student.situation:
            sit = "Approved"
        else:
            sit = "Reproved"
        f.write(student.name + " - " + sit + " with " + str(student.avg) +"\n")
    f.close()

def generate_statistics(data):
    stats = ClassroomStatistics(data)
    statistics_file = open("statistics.txt", "w")
    statistics_file.write(str(stats.get_students_count())+" students \n")
    statistics_file.write(str(stats.get_num_approvals())+" approved \n")
    statistics_file.write(str(stats.get_num_reproofs())+" reproved \n")
    statistics_file.write(str(stats.get_percent_of_approval()) + " percentual of approval \n")
    statistics_file.write(str(stats.get_avg_final_grade()) + " AVG final grade \n")
    statistics_file.close()

def menu(data):
    ans = None
    while ans != "5":
        print(" MENU ")
        print("1 - VALIDATE AVGs")
        print("2 - VALIDATE SITUATION")
        print("3 - GENERATE CLASS REPORT")
        print("4 - GENERATE STATISTICS")
        print("5 - EXIT \n")
        ans = input()
        if ans=="1":
            if (validate_avgs(data)):
                print("Sucessfull validation")
            else:
                print("AVGs are not valid, please check file")
            input()
        if ans=="2":
            if (validate_situation(data)):
                print("Sucessfull validation")
            else:
                print("Situations are not valid, please check file")
            input()
        if ans=="3":
            generate_class_report(data)
        if ans=="4":
            generate_statistics(data)
        
def main():
    print("Loading... "+FILE_NAME)
    myfile = open(FILE_NAME)
    raw_data = readfile(myfile)
    data = process_data(raw_data)
    print("DONE. \n")
    myfile.close()
    menu(data)

if __name__ == "__main__":
    main()

