class ClassroomStatistics:

    def __init__(self, data):
        self.data = data

    def get_students_count(self):
        return len(self.data)
    
    def get_num_approvals(self):
        count = 0
        for st in self.data:
            if (st.situation == True):
                count += 1
        return count

    def get_num_reproofs(self):
        count = 0
        for st in self.data:
            if (st.situation == False):
                count += 1
        return count

    def get_percent_of_approval(self):
        return self.get_num_approvals()/self.get_num_reproofs()

    def get_avg_final_grade(self):
        totalsum = 0
        for st in self.data:
            totalsum += st.avg
        return (totalsum/len(self.data))
        