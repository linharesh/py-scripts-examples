digraph G {

  node [fontname="Arial", shape=box, fontsize=29];
  edge [arrowsize=0.7];
  main [fillcolor=orangered, style=filled, shape=box];
  nodesep=0.18;
  ranksep=1;  
  ordering = out;
  compound=true;
  
  main -> open;
  main -> readfile;
  main -> process_data;
  main -> "myfile.close";
  main -> menu;
  
  {rank = same; open; readfile;process_data;menu;"myfile.close"} 

  parse_student1 [label="parse_student #1"];
  parse_student2 [label="parse_student #2"];
  parse_student3 [label="parse_student #3"];
  parse_student4 [label="parse_student #4"];
  parse_student5 [label="parse_student #5"];
      
  process_data -> parse_student1;
  process_data -> parse_student2;
  process_data -> parse_student3;
  process_data -> parse_student4;
  process_data -> parse_student5;
  
  
  Student1 [label="Student #1"];
  Student2 [label="Student #2"];
  Student3 [label="Student #3"];
  Student4 [label="Student #4"];
  Student5 [label="Student #5"];
  
  
  parse_student1 -> Student1;
  parse_student2 -> Student2;
  parse_student3 -> Student3;
  parse_student4 -> Student4;
  parse_student5 -> Student5;
  
  

  menu -> validate_situation;
  menu -> validate_avgs;
  menu -> generate_statistics;
  menu -> generate_class_report;
  
  
  generate_statistics -> get_students_count;
  generate_statistics -> get_num_approvals;
  generate_statistics -> get_num_reproofs;
  generate_statistics -> get_percent_of_approval;
  generate_statistics -> get_avg_final_grade;
  
  
  subgraph cluster_a {
    get_students_count;
    get_num_approvals;
    get_num_reproofs;
    get_percent_of_approval;
    get_avg_final_grade;        
    label = "ClassroomStatistics";
  }
  

  
}