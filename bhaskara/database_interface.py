import sqlite3
from Evaluation import Evaluation

class DatabaseInterface:

    def __init__(self, noworkflow_db_path):
        self.noworkflow_db_path = noworkflow_db_path
        self.connection = None
        self.cursor = None

    def connect(self):
        self.connection = sqlite3.connect(self.noworkflow_db_path)
        self.cursor = self.connection.cursor()

    def disconnect(self):
        self.connection.close()
        self.connection = None
        self.cursor = None

#    def get_trials(self):
#        self.connect()
#        query = "select * from trial"
#        self.cursor.execute(query)
#        trials = []
#        for row in self.cursor.fetchall():
#            tr = Trial(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9])
#            trials.append(tr)
#        self.disconnect()
#        return trials

    def get_evaluations(self, trial_id):
        self.connect()
        query = 'select * from evaluation ev where ev.trial_id = ? '
        trial_id = str(trial_id)
        print(trial_id)
        self.cursor.execute(query, [trial_id])
        evaluations = []
        for row in self.cursor.fetchall():
            evaluations.append(Evaluation(row[0], row[1], row[2], row[3], row[4]))
        self.disconnect()
        return evaluations


    def get_code_components(self, trial_id):
        self.connect()
        query = 'select * from code_component cc where cc.trial_id = ? '
        self.cursor.execute(query, [trial_id])
        code_components = []
        for row in self.cursor.fetchall():
            obj = {
                "trial_id": row[0],
                "id": row[1],
                "name": row[2],
                "type": row[3],
                "mode": row[4],
                "first_char_line": row[5],
                "first_char_column": row[6],
                "last_char_line": row[7],
                "last_char_column": row[8],
            }
            code_components.append(obj)
        self.disconnect()
        return code_components

        
