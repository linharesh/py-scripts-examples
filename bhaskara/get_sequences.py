import json
from config import Config

from database_interface import DatabaseInterface

def get_sequences():
    now_db_interface = DatabaseInterface(Config.NOWORKFLOW_DB_PATH)
    with open(Config.SECOND_EXECUTIONS_FILE_NAME) as json_file:
        executions = json.load(json_file)
    for ex in executions:
        evaluations = now_db_interface.get_evaluations(int(ex['execution_id']))
        code_component_sequence = [ev.code_component_id for ev in evaluations]
        ex['code_component_sequence'] = code_component_sequence


    f = open('3_executions.json', "w")
    f.write(json.dumps(executions, indent=2, default=str, ensure_ascii=False))
    f.close()
    
