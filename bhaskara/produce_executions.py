import random
import json
import os
import subprocess


from datetime import datetime

from config import Config

from progress.bar import Bar

def produce_executions():
    executions = []

    bar = Bar('Producing executions', fill='=', max=Config.NUMBER_OF_EXECUTIONS, suffix='%(percent)d%%')
    for i in range(1,Config.NUMBER_OF_EXECUTIONS+1):

        rand_a = random.randint(Config.A_RANGE_BEGIN, Config.A_RANGE_END)
        rand_b = random.randint(Config.B_RANGE_BEGIN, Config.B_RANGE_END)
        rand_c = random.randint(Config.C_RANGE_BEGIN, Config.C_RANGE_END)

        #call = ['python', 'bhaskara.py', str(rand_a), str(rand_b), str(rand_c)]    
        #proc = subprocess.Popen(call, cwd=os.getcwd(), env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        #stdout, stderr = proc.communicate()

        obj = {
            "execution_id": i,
            "input_a": rand_a,
            "input_b": rand_b,
            "input_c": rand_c,
            #"stdout": stdout.rstrip("\n"),
            #"stderr": stderr.rstrip("\n"),
            #"call": str(call),
            #"timestamp": str(datetime.now())
        }
        executions.append(obj)
        bar.next()
    bar.finish()
        
    f = open(Config.FIRST_EXECUTIONS_FILE_NAME, "w")
    f.write(json.dumps(executions, indent=2, default=str, ensure_ascii=False))
    f.close()