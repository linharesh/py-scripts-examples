import json
import subprocess
import os
from datetime import datetime
from progress.bar import Bar

from config import Config



def store_executions_with_now():
    
    executions_with_now = []
    with open(Config.FIRST_EXECUTIONS_FILE_NAME) as json_file:
        executions = json.load(json_file)
    print('len(executions): ', end='')
    print(len(executions))
    bar = Bar('Storing executions with NOW', fill='=', max=len(executions), suffix='%(percent)d%%')
    for e in executions:
        now_call = ['now','run', 'bhaskara.py', str(e['input_a']), str(e['input_b']), str(e['input_c'])]    
        proc = subprocess.Popen(now_call, cwd=os.getcwd(), env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout, stderr = proc.communicate()
        stdout = stdout.rstrip("\n")
        stderr = stderr.rstrip("\n")
        #same_result = e['stdout'] == stdout
        obj = {
            'execution_id': e['execution_id'],
            "input_a": e['input_a'],
            "input_b": e['input_b'],
            "input_c": e['input_c'],
            #"original_stdout": e['stdout'],
            #"original_stderr": e['stderr'],
            #"original_timestamp": e['timestamp'],
            #"original_call": e['call'],

            "now_stdout": stdout,
            "now_stderr": stderr,
            "now_call": str(now_call),
            "timestamp": str(datetime.now()),
            #"same_result": same_result
        }
        executions_with_now.append(obj)
        bar.next()
    bar.finish()
    f = open(Config.SECOND_EXECUTIONS_FILE_NAME, "w")
    f.write(json.dumps(executions_with_now, indent=2, default=str, ensure_ascii=False))
    f.close()

if __name__ == '__main__':
    store_executions_with_now()