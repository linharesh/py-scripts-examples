#!/usr/bin/env python
# coding: utf-8

# In[1]:


import json
from config import Config
import pandas as pd
from prefixspan import PrefixSpan


# In[2]:


with open('3_executions.json') as json_file:
    executions = json.load(json_file)


# In[3]:


df = pd.DataFrame.from_records(executions)


# In[4]:


#len(df[df.original_stdout == df.now_stdout])


# In[21]:


#data[0]


# In[23]:


data = list(df['code_component_sequence'])
mx = 0
for d in data:
    j = len(d)
    if j > mx:
        mx = j


# In[34]:


ps = PrefixSpan(data)
res = ps.topk(50)
f = open('topk_50.json', "w")
f.write(json.dumps(res, indent=2, default=str, ensure_ascii=False))
f.close()


# In[35]:


res2 = ps.frequent(5)

f = open('frequent_5.json', "w")
f.write(json.dumps(res2, indent=2, default=str, ensure_ascii=False))
f.close()
