import os
import shutil

from produce_executions import produce_executions
from store_executions_with_now import store_executions_with_now
from get_sequences import get_sequences
from config import Config


def run():
    if Config.DELETE_NOWORKFLOW_DIR and os.path.isdir(Config.NOWORKFLOW_DIR):
        shutil.rmtree(Config.NOWORKFLOW_DIR)
    produce_executions()
    store_executions_with_now()
    get_sequences()

if __name__ == '__main__':
    run()