import sys

def bhaskara(a,b,c):
    
    delta = (b ** 2) - 4 * a * c

    if a == 0:
        return("Error: 'a' must be > 0")
    elif delta < 0:
        return("NO REAL ROOTS")
    else:
        x1 = (-b + delta ** (1 / 2)) / (2 * a)
        x2 = (-b - delta ** (1 / 2)) / (2 * a)
        return(x1,x2)

if __name__ == '__main__':
    a, b, c = int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3])
    print(bhaskara(a,b,c))
    