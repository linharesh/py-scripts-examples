import json
from config import Config

from database_interface import DatabaseInterface

def get_code_components():
    now_db_interface = DatabaseInterface(Config.NOWORKFLOW_DB_PATH)
    code_components = now_db_interface.get_code_components(1)


    f = open('code_components.json', "w")
    f.write(json.dumps(code_components, indent=2, default=str, ensure_ascii=False))
    f.close()
    

if __name__ == '__main__':
    get_code_components()