
class Config:

    FIRST_EXECUTIONS_FILE_NAME = '1_executions.json'
    SECOND_EXECUTIONS_FILE_NAME = '2_executions.json'

    DELETE_NOWORKFLOW_DIR = True
    NOWORKFLOW_DIR = './.noworkflow'

    NOWORKFLOW_DB_PATH = './.noworkflow/db.sqlite'

    A_RANGE_BEGIN = -5000
    A_RANGE_END = 5000

    B_RANGE_BEGIN = -5000
    B_RANGE_END = 5000

    C_RANGE_BEGIN = -5000
    C_RANGE_END = 5000

    NUMBER_OF_EXECUTIONS = 70