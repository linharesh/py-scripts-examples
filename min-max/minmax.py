import csv

FILE_NAME = "MOCK_DATA.csv"

def leitura_csv(myfile):
    sample = []
    with myfile as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            sample.append(int(row[0]))
    return sample

def processa_dados(data):
    minimum = find_min(data)
    maximum = find_max(data)
    return minimum, maximum

def find_min(data):
    current_minimum = data[0]
    for d in data:
        if d < current_minimum:
            current_minimum = d
    return current_minimum

def find_max(data):
    current_maximum = data[0]
    for d in data:
        if d > current_maximum:
            current_maximum = d
    return current_maximum
    
def salva_resultado(result, filename):
    result_file = open(filename,"w")
    print(str(result)) 
    result_file.write(str(result))
    result_file.close()

def main():
    myfile = open(FILE_NAME)
    data = leitura_csv(myfile)
    proc_data = processa_dados(data)
    salva_resultado(proc_data, 'output.txt')
    myfile.close()

if __name__ == "__main__":
    main()