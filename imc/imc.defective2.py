import sys

def calc_imc(peso,altura):
    imc = peso / (altura * altura)
    return round(imc, 2)


def classify_imc(imc):
    result = None
    if imc <= 18.5:
        result = "Abaixo do peso"
    elif imc >= 18.6 and imc < 25:
        result = "No peso Ideal"
    elif imc >= 25 and imc < 30:
        result = "Levemente acima do peso"
    elif imc >= 30 and imc < 35:
        result = "Com obesidade grau I"   
    elif imc >= 35 and imc < 42: # defeituoso
        result = "Com obesidade grau II" # <<<< estamos apontando que o defeito está aqui (19)
    else:
        result = "Com obesidade grau III"
    return result
    

if __name__ == '__main__':
    peso, altura = float(sys.argv[1]), float(sys.argv[2])
    imc = calc_imc(peso,altura)
    print("IMC: "+str(imc),end=', ')
    answer = classify_imc(imc)
    print("Classificacao: "+str(answer))
    
