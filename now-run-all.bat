echo Running 1/6
cd age-avg
now run calculate.py
cd ..

echo Running 2/6
cd carstore
now run program.py
cd ..

echo Running 3/6
cd min-max
now run minmax.py
cd .. 

echo Running 4/6
cd s-example
now run program.py
cd ..

echo Running 5/6
cd statistics
now run program.py
cd ..

echo Running 6/6
cd text-processing
now run text-processing.py
cd ..

echo DONE
pause