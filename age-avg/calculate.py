import csv

FILE_NAME = "input.txt"

def average(numbers):
    total = sum(numbers)
    total = float(total)
    return total / len(numbers)

def read_csv(myfile):
    ages = []
    with myfile as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                ages.append(int(row[0]))
            line_count += 1
    print("Processed "+str(line_count)+" lines")
    return ages

def process_data(data):
    return calculate(data)

def calculate(data):
    return average(data)

def print_result(res):
    print("Average: "+str(res))

def open_file(name):
    return open(name)
    
def close_file(file):
    print("closing all files....")
    file.close()

def main():
    myfile = open_file(FILE_NAME)
    data = read_csv(myfile)
    result = process_data(data)
    print_result(result)
    close_file(myfile)

if __name__ == "__main__":
    main()