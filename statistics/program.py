import csv
import math

FILE_NAME = "MOCK_DATA.csv"

def process_data(data):
    minimum = find_min(data)
    maximum = find_max(data)
    summ = sum(data)
    avg = find_avg(data)
    variance = find_variance(data)
    std_deviation = find_std_deviation(data)
    print("Minimum: " + str(minimum))
    print("Maximum: " + str(maximum))
    print("Sum: " + str(summ))
    print("Avg: " + str(avg))
    print("Variance: " + str(variance))
    print("Standard Deviation: " + str(std_deviation))

def find_min(data):
    current_minimum = data[0]
    for d in data:
        if d < current_minimum:
            current_minimum = d
    return current_minimum

def find_max(data):
    current_maximum = data[0]
    for d in data:
        if d > current_maximum:
            current_maximum = d
    return current_maximum

def find_avg(data):
    return sum(data)/len(data)

def find_variance(data):
    avg = find_avg(data)
    summ = 0
    for num in data:
        summ += pow(num-avg,2)
    return summ

def find_std_deviation(data):
    variance = find_variance(data)
    return math.sqrt(variance)

def read_data(file_name):
    myfile = open(FILE_NAME)
    sample = []
    with myfile as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            sample.append(int(row[0]))
    myfile.close()
    return sample

#def print_result(data):
#    print("Minimum: {}".format(data['minimum']))
#    print("Maximum: {}".format(data['maximum']))
#    print("Sum: {}".format(data['sum']))
#    print("Avg: {}".format(data['avg']))
#    print("Variance: {}".format(data['variance']))
#    print("Standard Deviation: {}".format(data['std_deviation']))


def main():
    data = read_data(FILE_NAME)
    process_data(data)

if __name__ == "__main__":
    main()