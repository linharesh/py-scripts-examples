import requests
import json

FILE_NAME = "hosts.txt"
ADDRESS = 'http://raw.githubusercontent.com/linharesh/myapp-json-reference/master/content.json'

def get_request(address):
    r = requests.get(address)
    print(r.status_code)
    print(r.headers)
    return r.json()

def process(phrase):
    return phrase.replace('a','@').replace('A','@')

def save_content(data):
    file_name = data["file_name"]
    result_file = open(file_name,"w")
    result_file.write(data)
    result_file.close()

content = get_request(ADDRESS)
processed_content = process(content['data'])
save_content(processed_content)
