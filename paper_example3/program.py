import csv
import sys
import math

FUNCS = ['max', 'sum']

def read_args(args):
    obj = {}
    arr = []
    for idx,arg in enumerate(args):
        if idx == 1:
            obj['func'] = arg
        elif idx > 1:
            arr.append(float(arg))
    obj['arr'] = arr
    return obj

def assign(func, data):
    if func == 'max':
        return get_max(data)
    elif func == 'sum':
        return get_sum(data)
    else:
        return None


def get_max(data):
    current = data[0]
    for d in data:
        if d > current:
            current = math.inf
    return current

def get_sum(data):
    result = 0
    for i in data:
        result = result + i
    return result

def print_results(func, result):
    print("Function: {}".format(func))
    print("result: {}".format(result))

data = read_args(sys.argv)
result = assign(data['func'], data['arr'])
print_results(data['func'], result)
