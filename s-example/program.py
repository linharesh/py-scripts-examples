import csv

FILE_NAME = "MOCK_DATA.csv"

def leitura_csv(file_name):
    sample = []
    myfile = open(file_name)
    with myfile as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            sample.append(int(row[0]))
    myfile.close()
    return sample

def processa_dados(data):
    return sum(data)
    
def salva_resultado(result, filename):
    result_file = open(filename,"w")
    print(str(result)) 
    result_file.write(str(result))
    result_file.close()

def main():
    data = leitura_csv(FILE_NAME)
    proc_data = processa_dados(data)
    salva_resultado(proc_data, 'output.txt')

if __name__ == "__main__":
    main()