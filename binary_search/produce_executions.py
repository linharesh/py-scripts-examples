import random
import json
import os
import subprocess
from icecream import ic

from datetime import datetime

from config import Config

from log import log

def produce_executions():
    log("produce_executions() STARTED")
    executions = []
    for i in range(1,Config.NUMBER_OF_EXECUTIONS+1):
        numbers = random.sample(range(Config.ELEM_MIN_VALUE, Config.ELEM_MAX_VALUE), Config.LIST_SIZE)
        #ic(numbers)
        numbers.sort()
        x = random.choice(numbers)
        numbers_str = str(numbers).replace('[','').replace(']','').replace(' ','')
        call = ['python', 'binary_search.py', numbers_str, str(x)]    
        #ic(call)
        proc = subprocess.Popen(call, cwd=os.getcwd(), env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout, stderr = proc.communicate()
        obj = {
            "execution_id": i,
            "input_list": numbers_str,
            "input_x": x,
            "stdout": stdout.rstrip("\n"),
            "stderr": stderr.rstrip("\n"),
            "call": str(call),
            "timestamp": str(datetime.now())
        }
        executions.append(obj)        
    f = open("./data/1_execs.json", "w")
    f.write(json.dumps(executions, indent=2, default=str, ensure_ascii=False))
    f.close()
    log("produce_executions() FINISHED")
    