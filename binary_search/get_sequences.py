import json
from config import Config
from log import log
from database_interface import DatabaseInterface


def get_sequences():
    log("get_sequences() STARTED")
    now_db_interface = DatabaseInterface(Config.NOWORKFLOW_DB_PATH)
    with open('./data/2_execs.json') as json_file:
        executions = json.load(json_file)
    for ex in executions:
        evaluations = now_db_interface.get_evaluations(int(ex['execution_id']))
        code_component_sequence = [ev.code_component_id for ev in evaluations]
        ex['code_component_sequence'] = code_component_sequence
    f = open('./data/sequences.json', "w")
    f.write(json.dumps(executions, indent=2, default=str, ensure_ascii=False))
    f.close()
    log("get_sequences() FINISHED")