import os
import shutil

from produce_executions import produce_executions
from store_executions_with_now import store_executions_with_now
from get_sequences import get_sequences
from get_code_components import get_code_components
from config import Config
from log import log

def run():
    if Config.DELETE_NOWORKFLOW_DIR and os.path.isdir(Config.NOWORKFLOW_DIR):
        log("Deleting .noworkflow directory")
        shutil.rmtree(Config.NOWORKFLOW_DIR)
    produce_executions() 
    store_executions_with_now()
    get_sequences() 
    get_code_components() 

if __name__ == '__main__':
    run()