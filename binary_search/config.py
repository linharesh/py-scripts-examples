
class Config:

    ENABLE_LOG = True
    DELETE_NOWORKFLOW_DIR = True
    NOWORKFLOW_DIR = './.noworkflow'

    NOWORKFLOW_DB_PATH = './.noworkflow/db.sqlite'

    LIST_SIZE = 20

    ELEM_MIN_VALUE = 1
    ELEM_MAX_VALUE = 99
    

    NUMBER_OF_EXECUTIONS = 100