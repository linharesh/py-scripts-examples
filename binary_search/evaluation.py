class Evaluation:

    def __init__(self, trial_id, identifier, checkpoint, code_component_id, representation):
        self.trial_id = trial_id
        self.identifier = identifier
        self.checkpoint = checkpoint
        self.code_component_id = code_component_id
        self.representation = representation
