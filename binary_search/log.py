from datetime import datetime
from config import Config

def log(text):
    if Config.ENABLE_LOG:
        print("%s => %s " % (str(datetime.now()), text))
