import sys

def binary_search(arr, elem):
    low = 0
    high = len(arr) - 1
    mid = 0
    while low <= high:
        mid = (high + low) // 2
        if arr[mid] < elem:
            low = mid + 1
        elif arr[mid] > elem:
            high = mid - 1
        else:
            return mid
    return -1

def parse_args():
    argv_1 = sys.argv[1].split(',')
    arr = []
    for char in argv_1:
        arr.append(int(char))
    elem = int(sys.argv[2])
    return arr, elem
 
def main():
    arr, elem = parse_args()
    result = binary_search(arr, elem)
    print(result)

if __name__ == '__main__':
    main()
    