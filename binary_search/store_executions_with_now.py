import json
import subprocess
import os
from datetime import datetime
from config import Config
from log import log


def store_executions_with_now():
    log("store_executions_with_now() STARTED")
    executions_with_now = []
    with open('./data/1_execs.json') as json_file:
        executions = json.load(json_file)
    for e in executions:
        now_call = ['now','run', 'binary_search.py', str(e['input_list']), str(e['input_x'])]    
        proc = subprocess.Popen(now_call, cwd=os.getcwd(), env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        stdout, stderr = proc.communicate()
        stdout = stdout.rstrip("\n")
        stderr = stderr.rstrip("\n")
        #same_result = e['stdout'] == stdout
        obj = {
            'execution_id': e['execution_id'],
            "input_list": e['input_list'],
            "input_x": e['input_x'],
            #"original_stdout": e['stdout'],
            #"original_stderr": e['stderr'],
            #"original_timestamp": e['timestamp'],
            #"original_call": e['call'],

            "now_stdout": stdout,
            "now_stderr": stderr,
            "now_call": str(now_call),
            "timestamp": str(datetime.now()),
            #"same_result": same_result
        }
        executions_with_now.append(obj)
    f = open('./data/2_execs.json', "w")
    f.write(json.dumps(executions_with_now, indent=2, default=str, ensure_ascii=False))
    f.close()
    log("store_executions_with_now() FINISHED")
    

if __name__ == '__main__':
    store_executions_with_now()