import csv
import json

FILE_NAME = 'MOCK_DATA.csv'

def load_cars(file_name):
    cars = []
    myfile = open(file_name)
    with myfile as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            cars.append(row)
    return cars

def filter_by_year(year, cars):
    filtered_cars = []
    for c in cars:
        if int(c[3]) >= year:
            filtered_cars.append(c) 
    return filtered_cars
        
cars = load_cars(FILE_NAME)
ans = input("Type a year (for example 2006): ")
year = int(ans)
cars = filter_by_year(year, cars)
print(json.dumps(cars))
