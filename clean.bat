@echo OFF

cd age-avg 
IF EXIST .noworkflow (
    rd /s /q .noworkflow
    )
cd ..

cd carstore 
IF EXIST .noworkflow (
    rd /s /q .noworkflow
    )
cd ..

cd min-max 
IF EXIST .noworkflow (
    rd /s /q .noworkflow
    )
cd ..

cd s-example 
IF EXIST .noworkflow (
    rd /s /q .noworkflow
    )
cd ..

cd statistics 
IF EXIST .noworkflow (
    rd /s /q .noworkflow
    )
cd ..

cd text-processing 
IF EXIST .noworkflow (
    rd /s /q .noworkflow
    )
cd ..

echo DONE
pause