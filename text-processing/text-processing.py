
INPUT_FILE_NAME = "text.txt"
OUTPUT_FILE_NAME = "output.txt"

def process(content):
    return content.replace('a','@').replace('b','3').replace('.','!!')

def read_content(file_name):
    myfile = open(file_name)
    content = myfile.read()
    myfile.close()
    return content

def save_content(file_name, data):
    myfile = open(file_name, 'w')
    myfile.write(data)
    print(data)
    myfile.close()

content = read_content(INPUT_FILE_NAME)
out_data = process(content)
save_content(OUTPUT_FILE_NAME, out_data)