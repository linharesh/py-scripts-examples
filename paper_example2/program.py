from json import load
FILE_NAME = 'data.json'

def readfile(filename):
    f = open(filename, 'r')
    return load(f)

def find_min(data):
    current = float('inf')
    for d in data:
        if d < current:
            pass # Defective Line
    return current

def print_results(data, d_min):
    d_count = len(data)
    print(d_count)
    print(d_min)

print("Count - Min:")
data = readfile(FILE_NAME)
pdata = find_min(data)
print_results(data, pdata)